package hub

import (
	"eventStream/clientsession"

	"github.com/golang/protobuf/proto"
	"github.com/sirupsen/logrus"
	"gitlab.com/vajar_minigame/proto_go/gen/battle"
	"gitlab.com/vajar_minigame/proto_go/gen/common"
	"gitlab.com/vajar_minigame/proto_go/gen/ctrlmsg"
	"gitlab.com/vajar_minigame/proto_go/gen/item"
	"gitlab.com/vajar_minigame/proto_go/gen/monster"
	"gitlab.com/vajar_minigame/proto_go/gen/quest"

	"sync"
)

type EventReceiver interface {
	ReceiveItemEvent(event *item.ItemEvent) error
	ReceiveMonsterEvent(event *monster.MonsterEvent) error
	ReceiveQuestEvent(event *quest.QuestEvent) error
}

// Hub manages all websocket connections
type Hub struct {
	//receiver are the incoming events that need to be broadcasted
	receiver     []EventReceiver
	grpcClients  []clientsession.GrpcClient
	clientsMutex sync.RWMutex
}

func NewHub() *Hub {
	return &Hub{receiver: make([]EventReceiver, 0), grpcClients: make([]clientsession.GrpcClient, 0)}
}

func (h *Hub) AddReceiver() {

}

func (h *Hub) GetGrpcClient(id *common.UserId) clientsession.GrpcClient {
	//identify client session with userid for the moment

	h.clientsMutex.RLock()
	defer h.clientsMutex.RUnlock()

	for _, cI := range h.grpcClients {
		cID := cI.GetID()
		if proto.Equal(cID, id) {
			return cI
		}
	}

	client, _ := clientsession.NewGrpcClient(h, id)
	h.grpcClients = append(h.grpcClients, client)

	return client

}

func (h *Hub) BroadCastMonsterEvent(event *monster.MonsterEvent) {

	eventMsg := ctrlmsg.Event{Event: &ctrlmsg.Event_MonEvent{MonEvent: event}}
	h.broadCastMessage(&eventMsg)

}

func (h *Hub) BroadCastQuestEvent(event *quest.QuestEvent) {
	eventMsg := ctrlmsg.Event{Event: &ctrlmsg.Event_QuestEvent{QuestEvent: event}}
	h.broadCastMessage(&eventMsg)

}

func (h *Hub) BroadCastBattleEvent(event *battle.BattleEvent) {
	eventMsg := ctrlmsg.Event{Event: &ctrlmsg.Event_BattleEvent{BattleEvent: event}}
	h.broadCastMessage(&eventMsg)

}

func (h *Hub) BroadCastItemEvent(event *item.ItemEvent) {

	eventMsg := ctrlmsg.Event{Event: &ctrlmsg.Event_ItemEvent{ItemEvent: event}}
	h.broadCastMessage(&eventMsg)
}

func (h *Hub) broadCastMessage(event *ctrlmsg.Event) {
	go func() {

		h.clientsMutex.RLock()

		for _, client := range h.grpcClients {

			err := client.SendEvent(event)
			if err != nil {
				logrus.Warnf("error occured sending event %v", err)
			}
		}
		h.clientsMutex.RUnlock()

	}()
}
