package config

import (
	"io/ioutil"
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	DbConnect   string `yaml:"db_connect"`
	NatsConnect string `yaml:"nats_connect"`
	PrivKey     string `yaml:"priv_key"`
}

var config *Config

//LoadConfig .
func LoadConfig(filename string) *Config {
	var fname string
	environment := os.Getenv("BUILD_ENVIRONMENT")
	if environment == "PROD" {
		fname = filename + "Prod"
	} else {
		fname = filename
	}

	fname = fname + ".yaml"
	fileData, err := ioutil.ReadFile(fname)
	if err != nil {

		log.Fatal(err)
	}

	config = &Config{}

	err2 := yaml.Unmarshal(fileData, config)
	if err != nil {

		log.Fatal(err2)

	}

	return config
}

//GetConfig .
func GetConfig() *Config {
	if config == nil {
		log.Fatal("Load config first.")
	}
	return config
}
