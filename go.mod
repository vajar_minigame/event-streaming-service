module eventStream

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang/protobuf v1.4.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.0
	github.com/grpc-ecosystem/grpc-gateway v1.14.5 // indirect
	github.com/mattn/go-colorable v0.1.6
	github.com/nats-io/nats.go v1.10.0
	github.com/sirupsen/logrus v1.6.0
	gitlab.com/vajar_minigame/minigame_backend v0.0.0-20200519112721-c08acb6c3e6a
	gitlab.com/vajar_minigame/proto_go v0.0.0-20200425172725-2c0cd1c54187
	golang.org/x/net v0.0.0-20200519113804-d87ec0cfa476 // indirect
	golang.org/x/sys v0.0.0-20200519105757-fe76b779f299 // indirect
	google.golang.org/grpc v1.29.1
	gopkg.in/yaml.v2 v2.3.0
)
