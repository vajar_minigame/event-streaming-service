package controller

import (
	"gitlab.com/vajar_minigame/minigame_backend/config"
)

type Server struct {
	config *config.Config
}

func NewServer(config *config.Config) Server {
	return Server{config: config}

}

/*
func NewWSServer(db DBs, config *config.Config, controller *Controller) WSServer {
	return WSServer{db: db, config: config, controller: controller}
}


*/
