package controller

import (
	"errors"
	"eventStream/hub"

	"gitlab.com/vajar_minigame/proto_go/gen/common"
	events "gitlab.com/vajar_minigame/proto_go/gen/streaming"

	"github.com/sirupsen/logrus"
)

type EventService struct {
	hub *hub.Hub
}

func NewEventService(hub *hub.Hub) *EventService {

	return &EventService{hub: hub}

}

func (s *EventService) EventStream(req *common.UserId, srv events.EventStreamingService_EventStreamServer) error {

	//listen to a channel an send to client
	logrus.Debugf("new eventstreaming channel opened")
	eventChan := s.hub.GetGrpcClient(req).SetEventStream()

	for {
		select {
		case v, ok := <-eventChan:
			if ok == false {
				return errors.New("channel was closed")
			}
			err := srv.Send(v)
			if err != nil {
				logrus.Debugf("error when sending value. %v", err)
				s.hub.GetGrpcClient(req).ResetEventStream()
				return err
			}
		case <-srv.Context().Done():
			logrus.Debugf("the stream was closed with error %v", srv.Context().Err())
			s.hub.GetGrpcClient(req).ResetEventStream()
			return srv.Context().Err()

		}
	}
}
