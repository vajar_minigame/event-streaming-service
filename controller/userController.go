package controller

import (
	"eventStream/hub"

	"gitlab.com/vajar_minigame/minigame_backend/config"
)

//UserService dependencies
type UserService struct {
	config *config.Config
	hub    *hub.Hub
}

// NewUserService creates new userservice
func NewUserService(config *config.Config, hub *hub.Hub) *UserService {

	return &UserService{config: config, hub: hub}

}
