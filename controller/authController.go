package controller

import (
	"context"
	"crypto/rsa"
	"errors"
	"fmt"
	"io/ioutil"
	"time"

	"gitlab.com/vajar_minigame/proto_go/gen/common"

	"github.com/dgrijalva/jwt-go"
	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"github.com/sirupsen/logrus"
	"gitlab.com/vajar_minigame/minigame_backend/config"
	"gitlab.com/vajar_minigame/minigame_backend/controller/msg"
	"gitlab.com/vajar_minigame/minigame_backend/storage"
	"gitlab.com/vajar_minigame/proto_go/gen/auth"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

type authService struct {
	db      storage.UserDB
	config  *config.Config
	privKey *rsa.PrivateKey
}

//AuthHandler responsible for legitimating the client
type AuthHandler interface {
	Login(ctx context.Context, params *auth.LoginCall) (*auth.LoginResponse, error)
	Logout(ctx context.Context, params *auth.LogoutCall) (*auth.LogoutResponse, error)
	AuthFuncOverride(ctx context.Context, fullMethodName string) (context.Context, error)
	Authenticate(ctx context.Context) (context.Context, error)
}

//NewAuthService creates a new authservice
func NewAuthService(userDB storage.UserDB, config *config.Config) (AuthHandler, error) {

	//load private key to sign stuff
	key, err := ioutil.ReadFile(config.PrivKey)
	if err != nil {
		errMsg := fmt.Errorf("Error parsing the jwt private key: %s", err)
		logrus.Errorf(errMsg.Error())
		return nil, errMsg
	}
	parsedKey, err := jwt.ParseRSAPrivateKeyFromPEM(key)
	if err != nil {
		errMsg := fmt.Errorf("Error parsing the jwt private key: %s", err)
		logrus.Errorf(errMsg.Error())
		return nil, errMsg
	}
	err = parsedKey.Validate()
	if err != nil {
		logrus.Debugf(err.Error())
	}

	return &authService{db: userDB, config: config, privKey: parsedKey}, nil

}

func (s *authService) parseToken(token string) (*jwt.Token, error) {

	return jwt.ParseWithClaims(token, &msg.UserClaim{}, func(token *jwt.Token) (interface{}, error) {
		return &s.privKey.PublicKey, nil
	})

}

func (s *authService) Authenticate(ctx context.Context) (context.Context, error) {

	token, err := grpc_auth.AuthFromMD(ctx, "Bearer")

	if err != nil {
		// for debugging
		logrus.Debug("return no error even though not authenticated")
		return ctx, nil

		//logrus.Debug(err)
		//return nil, grpc.Errorf(codes.Unauthenticated, "invalid auth token: %v", err)
	}
	tokenInfo, err := s.parseToken(token)
	if err != nil {
		logrus.Debug(err)
		return nil, grpc.Errorf(codes.Unauthenticated, "invalid auth token: %v", err)
	}

	userClaim := tokenInfo.Claims.(*msg.UserClaim)

	newCtx := context.WithValue(ctx, "tokenInfo", tokenInfo)
	newCtx = context.WithValue(ctx, "user", userClaim)

	return newCtx, nil

}

func (s *authService) AuthFuncOverride(ctx context.Context, fullMethodName string) (context.Context, error) {

	return ctx, nil

}

//Login check username, password and send a token?
func (s *authService) Login(ctx context.Context, parameters *auth.LoginCall) (*auth.LoginResponse, error) {

	user, err := s.db.GetUserByUsername(ctx, parameters.GetUsername())
	if err != nil {
		logrus.Debugf("error getting user %s", err.Error())
		return nil, errors.New("user not found")
	}

	expirationTime := time.Now().Add(20 * time.Minute)
	// Create the Claims
	claims := msg.UserClaim{
		UserId: &common.UserId{Id: user.Id},
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
			Issuer:    "minigame",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	ss, err := token.SignedString(s.privKey)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	response := &auth.LoginResponse{User: user, Token: ss}
	return response, nil

}

//Logout at the moment do nothing, maybe invalidate token?
func (s *authService) Logout(ctx context.Context, parameter *auth.LogoutCall) (*auth.LogoutResponse, error) {

	_, err := s.db.GetUserByID(ctx, parameter.GetUserId())
	if err != nil {
		logrus.Debugf("error getting user %s", err.Error())
		return nil, errors.New("user not found")
	}

	response := &auth.LogoutResponse{Successful: true}
	return response, nil

}
