package clientsession

import (
	"errors"

	"gitlab.com/vajar_minigame/proto_go/gen/ctrlmsg"

	"gitlab.com/vajar_minigame/proto_go/gen/battle"
	"gitlab.com/vajar_minigame/proto_go/gen/common"
	"gitlab.com/vajar_minigame/proto_go/gen/item"
	"gitlab.com/vajar_minigame/proto_go/gen/monster"
	"gitlab.com/vajar_minigame/proto_go/gen/quest"
)

type grpcClient struct {
	hub Hub

	//channels to send events
	itemStream    chan *item.ItemEvent
	monsterStream chan *monster.MonsterEvent
	questStream   chan *quest.QuestEvent
	battleStream  chan *battle.BattleEvent
	// Buffered channel of outbound messages.

	eventStream chan *ctrlmsg.Event
	userID      *common.UserId
}

type Hub interface {
}

type GrpcClient interface {
	GetID() *common.UserId
	SendEvent(event *ctrlmsg.Event) error
	SetEventStream() chan *ctrlmsg.Event
	ResetEventStream()
}

func (c *grpcClient) ResetEventStream() {
	c.eventStream = nil
}

// checks if all streams are not set then delete itself
func (c *grpcClient) CheckIfNil() {

}

func (c *grpcClient) SetEventStream() chan *ctrlmsg.Event {
	if c.eventStream != nil {
		close(c.eventStream)
	}
	stream := make(chan *ctrlmsg.Event)
	c.eventStream = stream
	return stream
}

func (c *grpcClient) GetID() *common.UserId {
	return c.userID
}

func NewGrpcClient(h Hub, id *common.UserId) (GrpcClient, error) {

	client := grpcClient{
		hub:    h,
		userID: id,
	}
	return &client, nil

}

func (c *grpcClient) SendItemEvent(event *item.ItemEvent) error {

	if c.itemStream == nil {
		//todo check if nothing is set then delete client
		return errors.New("itemstream is not set")
	}

	c.itemStream <- event
	return nil

}

func (c *grpcClient) SendMonsterEvent(event *monster.MonsterEvent) error {

	if c.monsterStream == nil {
		//todo check if nothing is set then delete client
		return errors.New("monstream is not set")
	}
	c.monsterStream <- event
	return nil
}

func (c *grpcClient) SendQuestEvent(event *quest.QuestEvent) error {

	if c.questStream == nil {
		//todo check if nothing is set then delete client
		return errors.New("itemstream is not set")
	}

	c.questStream <- event
	return nil
}
func (c *grpcClient) SendBattleEvent(event *battle.BattleEvent) error {

	if c.battleStream == nil {
		//todo check if nothing is set then delete client
		return errors.New("itemstream is not set")
	}

	c.battleStream <- event
	return nil
}

func (c *grpcClient) SendEvent(event *ctrlmsg.Event) error {

	if c.eventStream == nil {
		//todo check if nothing is set then delete client
		return errors.New("eventStream is not set")
	}

	c.eventStream <- event
	return nil
}
