package router

import (
	"eventStream/config"
	"eventStream/controller"
	"eventStream/hub"
	"eventStream/receiver"
	"log"
	"net"

	events "gitlab.com/vajar_minigame/proto_go/gen/streaming"

	"google.golang.org/grpc"

	"github.com/sirupsen/logrus"
)

type server struct {
	hub    *hub.Hub
	config *config.Config
}

const address = ":8080"

func startNatsReceiver(config *config.Config, hub *hub.Hub) receiver.NatsReceiver {

	nats, err := receiver.NewNatsReceiver(config, hub)
	if err != nil {
		logrus.Fatal("cant connect to nats")
	}
	return nats
}

func startGRPC(config *config.Config, eventService *controller.EventService) {
	lis, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	grpcServer := grpc.NewServer()

	events.RegisterEventStreamingServiceServer(grpcServer, eventService)
	err = grpcServer.Serve(lis)
	if err != nil {
		logrus.Fatal(err)
	}
}

func StartServer() {
	config := config.LoadConfig("resources/config")

	logrus.SetLevel(logrus.TraceLevel)
	logrus.SetReportCaller(true)

	hub := hub.NewHub()

	logrus.Debug("connecting to nats")
	nats := startNatsReceiver(config, hub)
	err := nats.Start()
	if err != nil {
		logrus.Fatal(err)
	}

	eventService := controller.NewEventService(hub)

	startGRPC(config, eventService)

}
