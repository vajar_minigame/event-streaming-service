package receiver

import (
	"eventStream/config"

	"gitlab.com/vajar_minigame/proto_go/gen/battle"
	"gitlab.com/vajar_minigame/proto_go/gen/item"
	"gitlab.com/vajar_minigame/proto_go/gen/monster"
	"gitlab.com/vajar_minigame/proto_go/gen/quest"

	"github.com/golang/protobuf/proto"
	"github.com/nats-io/nats.go"
	"github.com/sirupsen/logrus"
)

type EventType int

const (
	ItemEvent EventType = iota
	MonsterEvent
	QuestEvent
	BattleEvent
)

func (e EventType) String() string {
	var events = [...]string{
		"ItemEvent",
		"MonsterEvent",
		"QuestEvent",
		"BattleEvent"}

	return events[e]
}

type Hub interface {
	BroadCastItemEvent(event *item.ItemEvent)
	BroadCastMonsterEvent(event *monster.MonsterEvent)
	BroadCastQuestEvent(event *quest.QuestEvent)
	BroadCastBattleEvent(event *battle.BattleEvent)
}

type NatsReceiver interface {
	Start() error
	Stop() error
}

type Natsclient struct {
	hub             Hub
	conn            *nats.Conn
	subscription    *nats.Subscription
	natsItemChan    chan *nats.Msg
	natsMonsterChan chan *nats.Msg
	natsQuestChan   chan *nats.Msg
	natsBattleChan  chan *nats.Msg
}

func NewNatsReceiver(config *config.Config, hub Hub) (NatsReceiver, error) {
	nc, err := nats.Connect(config.NatsConnect)
	if err != nil {
		logrus.Errorf("cant connect to nats: %v", err)
		return nil, err
	}

	client := Natsclient{conn: nc,
		hub: hub}
	return &client, nil
}

func (c *Natsclient) Start() error {

	ch := make(chan *nats.Msg, 64)
	_, err := c.conn.ChanSubscribe(EventType.String(ItemEvent), ch)
	if err != nil {
		return err
	}
	c.natsItemChan = ch

	ch = make(chan *nats.Msg, 64)
	_, err = c.conn.ChanSubscribe(EventType.String(MonsterEvent), ch)
	if err != nil {
		return err
	}
	c.natsMonsterChan = ch

	ch = make(chan *nats.Msg, 64)
	_, err = c.conn.ChanSubscribe(EventType.String(QuestEvent), ch)
	if err != nil {
		return err
	}
	c.natsQuestChan = ch

	ch = make(chan *nats.Msg, 64)
	_, err = c.conn.ChanSubscribe(EventType.String(BattleEvent), ch)
	if err != nil {
		return err
	}
	c.natsBattleChan = ch

	go c.readLoopItem()
	go c.readLoopMonster()
	go c.readLoopQuest()
	go c.readLoopBattle()
	return nil
}

func (c *Natsclient) Stop() error {
	return nil

}

func (c *Natsclient) readLoopItem() {

	for {
		select {
		case message, ok := <-c.natsItemChan:
			if ok == false {
				logrus.Error("something with channel")
			}

			pb := &item.ItemEvent{}
			err := proto.Unmarshal(message.Data, pb)
			if err != nil {
				logrus.Errorf("error parsing msg: %v", err)
				continue
			}

			c.hub.BroadCastItemEvent(pb)
		}
	}

}
func (c *Natsclient) readLoopMonster() {

	for {
		select {
		case message, ok := <-c.natsMonsterChan:
			if ok == false {
				logrus.Error("something with channel")
			}

			pb := &monster.MonsterEvent{}
			err := proto.Unmarshal(message.Data, pb)
			if err != nil {
				logrus.Errorf("error parsing msg: %v", err)
				continue
			}

			c.hub.BroadCastMonsterEvent(pb)
		}
	}

}
func (c *Natsclient) readLoopQuest() {

	for {
		select {
		case message, ok := <-c.natsQuestChan:
			if ok == false {
				logrus.Error("something with channel")
			}

			pb := &quest.QuestEvent{}
			err := proto.Unmarshal(message.Data, pb)
			if err != nil {
				logrus.Errorf("error parsing msg: %v", err)
				continue
			}

			logrus.Debug(pb)
			c.hub.BroadCastQuestEvent(pb)
		}
	}

}

func (c *Natsclient) readLoopBattle() {

	for {
		select {
		case message, ok := <-c.natsBattleChan:
			if ok == false {
				logrus.Error("something with channel")
			}

			pb := &battle.BattleEvent{}
			err := proto.Unmarshal(message.Data, pb)
			if err != nil {
				logrus.Errorf("error parsing msg: %v", err)
				continue
			}

			logrus.Debug(pb)
			c.hub.BroadCastBattleEvent(pb)
		}
	}

}
