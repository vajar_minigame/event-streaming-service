package main

import (
	"eventStream/router"

	"github.com/mattn/go-colorable"
	"github.com/sirupsen/logrus"
)

func main() {
	logrus.SetFormatter(&logrus.TextFormatter{ForceColors: true})
	logrus.SetOutput(colorable.NewColorableStdout())
	logrus.SetLevel(logrus.TraceLevel)

	router.StartServer()
}
